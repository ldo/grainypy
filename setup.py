#+
# Setuptools script to build and install the GrainyPy modules. Make
# sure setuptools <https://setuptools.pypa.io/en/latest/index.html> is
# installed. Invoke from the command line in this directory as
# follows:
#
#     python3 setup.py build
#     sudo python3 setup.py install
#
# Written by Lawrence D'Oliveiro <ldo@geek-central.gen.nz>.
#-

import setuptools

setuptools.setup \
  (
    name = "GrainyPy",
    version = "0.5",
    description = "Make High-Quality Images Look Grainy",
    author = "Lawrence D’Oliveiro",
    author_email = "ldo@geek-central.gen.nz",
    url = "https://gitlab.com/ldo/grainypy",
    py_modules = ["grainy"],
    ext_modules =
        [
            setuptools.Extension
              (
                name = "grainyx",
                sources = ["grainyx.c"],
                libraries = ["m"],
                extra_compile_args = ["-Wno-parentheses", "-Wno-maybe-uninitialized"],
              ),
        ],
  )
